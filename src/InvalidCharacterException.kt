class InvalidCharacterException
    (message: String?="Az állapotgép nem tudja elfogadni karaktersort.")
    : Throwable(message) {
}