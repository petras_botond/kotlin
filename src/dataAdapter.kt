class dataAdapter {
    companion object{
        @JvmStatic fun stringToData(strings:MutableList<String>):MutableList<dataStruct>{
            val data : MutableList<dataStruct> = mutableListOf()

            var temp:dataStruct? = null
            var name =""
            var start = false
            var stop = false

            var ml: MutableList<String>?
            var p: Pair<String, MutableList<String>>?

            var out:MutableList<Pair<String,MutableList<String>>>? = null

            for(i in 0..strings.indexOf(strings.last())){
                if(strings[i][0] !=' '){
                    if(temp != null){
                        data.add(temp)
                    }
                        //(id start stop)
                    val spl = strings[i].split(' ')

                    name=spl[0]
                    start = spl[1].toInt() == 1
                    stop = spl[2].toInt() == 1
                }
                else{
                    var x = strings[i].substring(1)
                    val spl = x.split(' ')
                    if(out == null){
                        ml = mutableListOf(spl[1])
                        p = Pair(spl[0],ml)
                        out = mutableListOf(p)
                    }
                    else{
                        var benne =false
                        for( o in 0..out.indexOf(out.last())){
                            if(out[o].first == spl[0]){
                                out[o].second.add(spl[1])
                                benne= true
                            }
                        }
                        if(!benne){
                            ml = mutableListOf(spl[1])
                            p = Pair(spl[0],ml)
                            out.add(p)
                        }
                    }
                }

                if(strings[i][0] ==' ' && strings[i+1][0] !=' ') {
                    var max: ULong = 0u
                    for (o in 0..out!!.indexOf(out.last())) {
                        if (out[o].first.length.toULong() > max) max = out[o].first.length.toULong()
                    }

                    temp = dataStruct(name, start, stop, max, out!!)
                    data.add(temp)
                }
            }

            return data
        }

        @JvmStatic fun dataToString(data:MutableList<Array<String>>):MutableList<String>{
            val strings : MutableList<String> = mutableListOf()
            if(data.size != 0){
                for(i in 0..data.indexOf(data.last())){
                    var temp =""
                    for(o in 0..data[i].size){
                        temp += data[i][o]
                    }
                    strings.add(temp)
                }
            }
            return strings
        }
    }
}