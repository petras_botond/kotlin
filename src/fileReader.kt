import java.io.File
import java.io.InputStream


class fileReader(inputName:String) {
//    Input methode
    private val input : InputStream
    //private var x: String
    private var list :MutableList<String> = mutableListOf()

    init {
        input = File(inputName).inputStream()

        input.bufferedReader().lines().forEach {
            if(it != " "){
                list.add(it)
            }
        }

    }

    fun getStrings(): MutableList<String> {
        return list
    }
}