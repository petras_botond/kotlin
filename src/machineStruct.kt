class machineStruct {
    private val start:dataStruct
    private val states:MutableList<dataStruct>
    private var route:MutableList<Array<String>> = mutableListOf()

    constructor(
        states: MutableList<dataStruct>,
        start: dataStruct
    ) {
        this.states = states
        this.start = start
    }

    fun getStart():dataStruct{
        return start
    }

    fun getRoute(): MutableList<Array<String>>? {
        return route
    }

    fun startRoute(input:String){
        var goal = false
        var maxChar =input.length
        var counter = 0
        var charCount = 0
        var open:MutableList<segedAdat<Int,Int,dataStruct>> = mutableListOf() //openlista az útvonalkereséshez (segédlet a 2. féléves mesterséges intelligencia alapjai c. tárgyon)
        var currentState:segedAdat<Int,Int,dataStruct>? = null
            //kiolvasott karakterek száma, kiolvasási index, aktuális állapot
        var tempRoute:Array<String> = arrayOf() //az indexelhetőségért
        var emptyList:MutableList<String> = mutableListOf() //visszatérés ellenőrzéséhez
        var tempPair:segedAdat<Int,Int,dataStruct> = segedAdat(0,0,start)

        open.add(tempPair) //openlist kezdeti állapota (1db) a start állapot
        while (open.size!=0){
            currentState=open[0]
            open.removeAt(0)
            counter=0
            charCount += currentState.second
            if(charCount<=maxChar) {
                if (currentState.third.getMaxOut() > 0u) {
                    var temp = ""
                    for (i in 0.toULong()..currentState.third.getMaxOut()) {
                        for (o in 0.toULong()..i) {
                            temp += input[currentState.first + o.toInt()]
                        }
                        if (currentState.third.getOutX(temp) != emptyList) {
                            tempPair.second = currentState.second + i.toInt()

                            if(currentState.third.getOutX(temp) != null){
                                currentState.third.getOutX(temp)!!.toList().forEach {
                                    var t:dataStruct? = null
                                    states.forEach { ot ->
                                        if(ot.getName()==it){
                                            t=ot
                                        }
                                    }
                                    if(t != null){
                                        tempPair.third = t!!
                                        open.add(0, tempPair)
                                        counter++
                                    }
                                }
                            }
                        }

                        temp = ""
                    }
                    tempPair.first = currentState.first
                    if(currentState.third.getOutX(temp) != null) {
                        currentState.third.getOutX("_")!!.toList().forEach {
                            //A "".helyére az empty él karakterét kell majd tenni
                            var t: dataStruct? = null
                            states.forEach { ot ->
                                if (ot.getName() == it) {
                                    t = ot
                                }
                            }
                            tempPair.third = t!!
                            open.add(0, tempPair)
                            counter++
                        }
                    }
                } else {
                    tempPair.first = currentState.first
                    if(tempPair.third.getOutX("_") != null){
                        currentState.third.getOutX("_")!!.forEach {
                            //A "".helyére az empty él karakterét kell majd tenni
                            var t:dataStruct? = null
                            states.forEach { ot ->
                                if(ot.getName()==it){
                                    t=ot
                                }
                            }
                            tempPair.third = t!!
                            open.add(0, tempPair)
                            counter++
                        }
                    }
                }
            }
            else{
                tempPair.first = currentState.first
                if(currentState.third.getOutX("_") != null) {
                    currentState.third.getOutX("_")!!.toList().forEach {
                        //A "".helyére az empty él karakterét kell majd tenni
                        var t: dataStruct? = null
                        states.forEach { ot ->
                            if (ot.getName() == it) {
                                t = ot
                            }
                        }
                        tempPair.third = t!!
                        open.add(0, tempPair)
                        counter++
                    }
                }
            }
            tempRoute.plus(currentState.third.getName())
            if(counter==0){
                if(currentState.third.getEnd()){
                    goal=true
                    route.add(tempRoute)
                    charCount=open[0].first
                    for(i in open[0].second..tempRoute.size){
                        tempRoute.dropLast(1)
                    }
                }
                else{
                    if(open.size !=0 ) {
                        charCount = open[0].first
                        for (i in open[0].second..tempRoute.size) {
                            tempRoute.dropLast(1)
                        }
                    }
                }
            }
            if(charCount>=maxChar*2){
                if(open.isNotEmpty()){
                    charCount=open[0].first
                    for(i in open[0].second..tempRoute.size){
                        tempRoute.dropLast(1)
                    }
                }
            }
        }
        if(goal){
            println("it's ok")
        }
        //machine.cpp ~90 soros stratRoute függvényének implementációja
    }
}