class dataStruct:Comparable<dataStruct>{
    override fun compareTo(other: dataStruct) = compareValuesBy(this, other, {it.name},{it.maxOut})

    private val name:String //adott állapot neve (példában számok 1-5)
    private val start:Boolean //az adott állapot kezdőállapot-e
    private val end:Boolean //az adott állapot végállapot-e
    private val maxOut:ULong //kimenő éleik karaktersorozata max hány karakteres
    private val out:MutableList<Pair<String,MutableList<String>>> //kimenő élek karakterei és végpontjai

    constructor(
        name: String = "",
        start: Boolean = false,
        end: Boolean = false,
        max_out: ULong = 0u,
        out: MutableList<Pair<String, MutableList<String>>>
    ) {
        this.name = name
        this.start = start
        this.end = end
        this.maxOut = max_out
        this.out = out
    }

    fun getName(): String {
        return this.name
    }

    fun getStart():Boolean{
        return this.start
    }

    fun getEnd():Boolean{
        return this.end
    }

    fun getMaxOut(): ULong {
        return this.maxOut
    }

    fun getOut(): MutableList<Pair<String, MutableList<String>>>? {
        return this.out
    }

    fun getOutX(x:String): MutableList<String>? {
        this.out.forEach {
            if (it.first == x) {
                return it.second
            }
        }
        return null
    }

}