import java.io.File
import java.io.OutputStream

class fileWriter (outputName:String){
    //    output methode
    private val output : OutputStream

    init {
        output = File(outputName).outputStream()
    }

    fun writeData(list :MutableList<String>){
        list.forEach {
            output.bufferedWriter().newLine()
            output.bufferedWriter().write(it)
        }
    }
}